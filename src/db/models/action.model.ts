import mongoose, { Document } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface IActionModel extends Document {
    _id: string;
    name: string;
    text: string;
    createdAt: Date;
    updatedAt: Date;
}

export const actionSchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        text: String,
    },
    { timestamps: true },
);

export const ActionModel = mongoose.model<IActionModel>('Action', actionSchema);

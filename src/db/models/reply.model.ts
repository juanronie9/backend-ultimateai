import mongoose, { Document, Schema } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface IReplyModel extends Document {
    _id: string;
    name: string;
    description: string;
    actionId: string;
    messages: any[];
    createdAt: Date;
    updatedAt: Date;
}

export const replySchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        description: String,
        actionId: { type: Schema.Types.ObjectId, ref: 'Action' },
        messages: [{ type: Schema.Types.ObjectId, ref: 'Message' }],
    },
    { timestamps: true },
);

export const ReplyModel = mongoose.model<IReplyModel>('Reply', replySchema);

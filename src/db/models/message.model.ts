import mongoose, { Document } from 'mongoose';
import { baseEntitySchema } from './base-entity-schema';

export interface IMessageModel extends Document {
    _id: string;
    name: string;
    text: string;
    createdAt: Date;
    updatedAt: Date;
}

export const messageSchema = new mongoose.Schema(
    {
        ...baseEntitySchema,
        text: String
    },
    { timestamps: true },
);

export const MessageModel = mongoose.model<IMessageModel>('Message', messageSchema);

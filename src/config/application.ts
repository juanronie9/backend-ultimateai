export default {
    httpPort: process.env.APP_HTTP_PORT,
    publicUrl: process.env.APP_PUBLIC_URL,
    environmentName: process.env.APP_ENVIRONMENT_NAME || 'local',
    log: {
        level: process.env.APP_LOG_LEVEL || 'debug',
    },
    mongoDb: {
        database: process.env.APP_MONGO_DATABASE,
        host: process.env.APP_MONGO_HOST,
        pass: process.env.MONGO_PASS,
        port: process.env.MONGO_PORT,
        srv: process.env.MONGO_SRV,
        user: process.env.APP_MONGO_USER,
    },
    appAdminAccessIPList: process.env.APP_ADMIN_ACCESS_IP_LIST,
    backendAi: {
        uri: process.env.APP_BACKEND_AI_URI || 'https://chat.ultimate.ai',
        token: process.env.APP_BACKEND_AI_TOKEN || '825765d4-7f8d-4d83-bb03-9d45ac9c27c0'
    },
};

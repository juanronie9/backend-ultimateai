import { MessageModel } from '../../db/models/message.model';

import { IPersistableEntity, PersistableEntity } from '../common';

export interface IMessage extends IPersistableEntity {
    text: string;
}

export class Message extends PersistableEntity implements IMessage {
    private _text: string;

    get text(): string {
        return this._text;
    }

    setText(value){
        this._text = value;
        return this;
    }

    toJSON() {
        const base = super.toJSON();

        return {
            ...base,
            text: this._text
        };
    }

    toModelValues() {
        const raw = this.toJSON();

        for (const key of Object.keys(raw)) {
            if (!raw[key]) delete raw[key];
        }

        return raw;
    }

    static fromRaw(
        raw: KeyValueHash | typeof MessageModel | any,
        options?: KeyValueHash,
    ): Message {
        const instance = new this();

        instance
            .setId(raw.id)
            .setCreatedAt(raw.createdAt)
            .setUpdatedAt(raw.updatedAt)
            .setText(raw.text)

        return instance;
    }
}

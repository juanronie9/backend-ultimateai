import { MessageModel } from '../../db/models/message.model';
import { MongoGateway } from '../common/persistance-gateway';
import { Message } from './message';

export class MessageGateway extends MongoGateway {
    private _messageModel = MessageModel;

    setMessageModel(model: typeof MessageModel) {
        this._messageModel = model;
        return this;
    }

    async createMessage(message: Message): Promise<Message> {
        const modelValues = {
            ...message.toModelValues(),
            createdAt: Date.now(),
            updatedAt: Date.now(),
        };

        const created = await this._messageModel.create(modelValues);

        return Message.fromRaw(created);
    }
}

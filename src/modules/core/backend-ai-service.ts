import request from 'request';
import applicationConfig from '../../config/application';
import { HttpStatus } from '../common';
import { logger } from '../../app';


export interface GetIntentsResponseObject {
    confidence: number;
    name: string;
}

export class BackendAiService {

    constructor() {}

    static async getIntents(botId: string, message: string): Promise<GetIntentsResponseObject[]> {
        const uri = `${applicationConfig.backendAi.uri}/api/intents`;
        const options = {
            method: 'POST',
            uri,
            headers: {
                'Authorization': applicationConfig.backendAi.token,
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            },
            body: {
                botId,
                message
            },
            json: true,
        };

        const res = await new Promise((resolve, reject) => {
            request(options, (err, response, body) => {
                if (err) {
                    logger.warn('Error during intents obtaining: ', err, body);
                    return reject(err);
                }

                if (response.statusCode >= HttpStatus.BAD_REQUEST) {
                    reject(
                        body ||
                            new Error(
                                `Unable to obtain intents. ErrorCode: ${response.statusCode}`,
                            ),
                    );
                }

                if(!body.intents) {
                    logger.info('getIntents: Wrong response received from UltimateAI Api, no intents property found', body);
                    return reject(new Error('Unexpected JSON from Api'));
                }

                return resolve(body.intents);
            });
        });

        return (Array.isArray(res) ? res : []);
    }
}

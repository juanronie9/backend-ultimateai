import { IReplyModel, ReplyModel } from '../../db/models/reply.model';
import { MongoGateway } from '../common/persistance-gateway';
import { Reply } from './reply';

interface ToReplyParameters {
    reply: IReplyModel;
}

interface UpdateReplyParameters {
    reply: Reply,
}

interface GetReplyByIdParameters {
    id: string,
}

interface GetReplyByNameParameters {
    name: string,
}

export class ReplyGateway extends MongoGateway {
    private _replyModel = ReplyModel;

    setReplyModel(model: typeof ReplyModel) {
        this._replyModel = model;
        return this;
    }

    async update({ reply }: UpdateReplyParameters) {
        const { id } = reply;
        const replyMessages = [...reply.messages];

        const messagesIds = (replyMessages as any[]).map(({ _id: messageId }) => messageId.toString() );

        const modelValues = {
            ...reply.toModelValues(),
            updatedAt: Date.now(),
            messages: messagesIds
        }

        const query = {
            _id: id,
        }
        // @ts-ignore
        await this._replyModel.updateOne(query, modelValues);

        return this.getReplyById({
            id: id.toString(),
        });
    }

    async getReplyById({ id }: GetReplyByIdParameters) {
        const query = {
            _id: id,
        };

        const reply = await this._replyModel
            .findOne(query)
            .populate('messages')
            .populate('actionId')
            .exec();

        if (!reply) {
            return null;
        }

        return reply;
    }

    async getReplyByName({ name }: GetReplyByNameParameters) {
        const query = {
            name
        };

        const reply = await this._replyModel
            .findOne(query)
            .populate('messages')
            .populate('actionId')
            .exec();

        if (!reply) {
            return null;
        }

        return this.toReply({ reply });
    }

    private async toReply({ reply }: ToReplyParameters): Promise<Reply> {
        return Reply.fromRaw(reply);
    }
}

import { Interactor, IRequestObject, BackendAiService } from '../core';
import joi from 'joi';
import { logger } from '../../app';
import { MessageGateway } from '../messages/message-gateway';
import { Message } from '../messages/message';
import { ReplyGateway } from './reply-gateway';

export interface GetRepliesRequestObject extends IRequestObject {
    botId: string,
    message: string,
}

export const validationSchema = joi.object().keys({
    botId: joi
        .string()
        .required(),
    message: joi
        .string()
        .required(),
});

const MIN_CONFIDENCE_THRESHOLD = 0.3;

export class GetRepliesInteractor extends Interactor {

    private _replyGateway: ReplyGateway = null;
    private _messageGateway: MessageGateway = null;

    setReplyGateway(gateway: ReplyGateway) {
        this._replyGateway = gateway;
        return this;
    }

    setMessageGateway(gateway: MessageGateway) {
        this._messageGateway = gateway;
        return this;
    }

    async execute(request: GetRepliesRequestObject) {
        try {

            const response = await BackendAiService.getIntents(request.botId, request.message);

            // get the best intent value in array objs
            const intent = response.reduce((prev, current) => (prev.confidence > current.confidence) ? prev : current);
            if (intent.confidence < MIN_CONFIDENCE_THRESHOLD) {
                return this.presenter.error('The AI could not give the correct answer');
            }

            // Check if exists this intent name in DB
            const reply = await this._replyGateway.getReplyByName({ name: intent.name });
            if (!reply) {
                return this.presenter.error('The AI could not give the correct answer');
            }

            // Create new message
            const messageRequest = {
                text: request.message,
            }
            const message = Message.fromRaw(messageRequest);
            const messageResult = await this._messageGateway.createMessage(message);

            // Update reply (insert messageId in array of messages)
            reply.addMessage(messageResult);
            const result = await this._replyGateway.update({ reply } );

            return this.presenter.success(result);
        } catch (e) {
            logger.error('Failed to create reply', request, e);
            return this.presenter.error([e.message]);
        }
    }
}

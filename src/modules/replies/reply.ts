import { ReplyModel } from '../../db/models/reply.model';

import { IPersistableEntity, PersistableEntity } from '../common';
import { MessageModel } from '../../db/models/message.model';
import { Message } from '../messages/message';

export interface IReply extends IPersistableEntity {
    description: string;
    actionId: KeyValueHash;
}

export class Reply extends PersistableEntity implements IReply {
    private _description: string;
    private _actionId: KeyValueHash;
    private readonly _messages = new Set<KeyValueHash | Message | typeof MessageModel>();


    get description(): string {
        return this._description;
    }

    setDescription(value){
        this._description = value;
        return this;
    }

    get actionId(): KeyValueHash {
        return this._actionId;
    }

    setActionId(action: KeyValueHash): Reply {
        this._actionId = action;
        return this;
    }

    get messages() {
        return [...this._messages];
    }

    addMessage(message: Message | typeof MessageModel): Reply {
        this._messages.add(message);
        return this;
    }

    toJSON() {
        const base = super.toJSON();

        return {
            ...base,
            description: this._description,
            actionId: this._actionId,
            messages: [this._messages],
        };
    }

    toModelValues() {
        const raw = this.toJSON();

        for (const key of Object.keys(raw)) {
            if (!raw[key]) delete raw[key];
        }
        return raw;
    }

    static fromRaw(
        raw: KeyValueHash | typeof ReplyModel | any,
        options?: KeyValueHash,
    ): Reply {
        const instance = new this();

        instance
            .setId(raw.id)
            .setName(raw.name)
            .setCreatedAt(raw.createdAt)
            .setUpdatedAt(raw.updatedAt)
            .setDescription(raw.description)
            .setActionId(raw.actionId)

        if (raw.messages) {
            (raw.messages as typeof MessageModel[]).forEach(message =>
                instance.addMessage(message),
            );
        }

        return instance;
    }
}

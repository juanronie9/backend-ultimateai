import { Router } from 'express';

import { RepliesController } from './replies-controller';

export class ApiRouter {

    constructor() {}

    build() {
        const router = Router();

        const repliesController = new RepliesController();

        router.get('/healthcheck', (req, res) => {
            const healthcheck = {
                uptime: process.uptime(),
                message: 'OK',
                timestamp: Date.now()
            };
            try {
                return res.status(200).send(healthcheck);
            } catch (e) {
                healthcheck.message = e;
                return res.status(503).send();
            }
        });

        // Replies api
        router.use('/replies', repliesController.router);

        return router;
    }
}

import { ExpressController, ExpressJSONShowPresenter } from '../../modules/core';

import { ReplyGateway } from '../../modules/replies/reply-gateway';
import { GetRepliesRequestObject, validationSchema as createSchema } from '../../modules/replies/get-replies-interactor';
import { GetRepliesInteractor } from '../../modules/replies/get-replies-interactor';
import { MessageGateway } from '../../modules/messages/message-gateway';

export class RepliesController extends ExpressController {
    constructor() {
        super();

        this.router.post('/', this.validator.validateBody(createSchema), this.getReplies.bind(this));
    }

    async getReplies(req, res, next) {
        const replyGateway = new ReplyGateway();
        const messageGateway = new MessageGateway();

        const interactor = new GetRepliesInteractor();

        const presenter = new ExpressJSONShowPresenter(req, res, next);

        const request = req.body as GetRepliesRequestObject;

        interactor.setPresenter(presenter).setMessageGateway(messageGateway).setReplyGateway(replyGateway);

        await interactor.execute(request);
    }
}

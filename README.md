# The Backend API

Provides a rich `backend-api` abilities.

## API

API is built on top of [OpenAPI 3.0 specification](https://swagger.io/specification/)
See: [swagger.yml](./src/templates/api-docs/swagger.yaml) for details.

#### Documentation
```
# DEV
http://localhost:3000/docs/api

# PROD
https://<domain>/docs/api
```

## Building

The project uses [TypeScript](https://github.com/Microsoft/TypeScript) as a JavaScript transpiler.
The sources are located under the `src` directory, the distributables are in the `dist`.

### Requirements:

-   Nodejs >= 8
-   MongoDB
-   Docker

### Environment:

```bash
APP_HTTP_PORT=3000
APP_PUBLIC_URL=http://localhost:3000
APP_ENVIRONMENT_NAME=
APP_MONGO_DATABASE=
APP_MONGO_HOST=
MONGO_PASS=
MONGO_PORT=
MONGO_SRV=
APP_MONGO_USER=
APP_ADMIN_ACCESS_IP_LIST=
APP_BACKEND_AI_URI=
APP_BACKEND_AI_TOKEN=
```

To make the application running use

```bash
npm run build
npm run start
```

To run application with docker:

```bash
docker-compose up api
## run with debug
docker-compose -f docker-compose.yml -f docker-compose.debug.yml up api
```

## Development

```bash
npm run debug
```

## Testing

It uses Jestjs and supertest, but it can be changed to other lib.

Open one terminal with project running and other terminal to execute the e2e tests.
```bash
npm run test
# or
docker-compose exec api npm test
```

---

## Application Structure

- [src](./src): The main `backend` application code.
    - [api](./src/api): Defines API routers and controllers
    - [config](./src/config): Defines configuration. Examples ENV_VARS
    - [db](./src/db): MongoDB models and schemas
    - [modules](./src/modules): Defines modules and logic of the application. 
      All the modules are composed of `Interactors` used by controllers, `Gateways` to interact with DB and `Services` to handle logic or request external REST API's.   
- [test](./test): Includes all the testing of the application
- [app.ts](./app.ts): Main file. Inject packages, initiate and bootstrap application

## MongoDB Schema 


#### replies

![mongodb collection replies](./src/templates/assets/images/replies.png)

#### messages
![mongodb collection messages](./src/templates/assets/images/messages.png)

#### actions
![mongodb collection actions](./src/templates/assets/images/actions.png)


## Next Steps

There are a lot of options to improve the application. 

-   Support multi-language
-   Increase the number of available actions (replies messages)
-   Set the type of the message. Example: expression, static message
-   Create admin api to manage (get, post, update, delete actions) and connect to a frontend app


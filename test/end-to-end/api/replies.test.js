const supertest = require('supertest');
const _ = require('lodash');


const request = supertest('http://0.0.0.0:3000');
const prefix = '/v1/';

function api(path) {
    return prefix + path;
}

const bodyRequest = {
    botId: '5f74865056d7bb000fcd39ff',
    message: 'Hello this is a chat message',
};

describe('Replies Test', () => {

    describe('Get replies [POST] /v1/replies', () => {

        it('should fail when body is not provided', async () => {
            const response = await request
                .post(api('replies'))
                .send({});

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters required missing', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            delete bodyRequestTest.botId;
            const response = await request
                .post(api('replies'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when body is not valid: parameters type not valid', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.botId = 1234;
            const response = await request
                .post(api('replies'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(400);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        it('should fail when data is not recognized by external AI or low confidence', async () => {
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.message = 'backend ultimate ai. The coding challenge :)'
            const response = await request
                .post(api('replies'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(404);
            const { body } = response;
            const { errors } = body;
            expect(errors.length).toBeGreaterThanOrEqual(1);
        });

        // static examples data
        const testData = [
            ['Hi', 'Greeting','Hello, How can I help you?'],
            ['Bye','Goodbye','Goodbye, have a nice day!'],
            ['Yes', 'Affirmative', 'Great!']
        ]

        test.each(testData)('should return replies. Message for message=%s type=%s  reply=%s',async (message, name, reply) =>{
            const bodyRequestTest = _.clone(bodyRequest);
            bodyRequestTest.message = message;
            const response = await request
                .post(api('replies'))
                .send(bodyRequestTest);

            expect(response.statusCode).toBe(200);
            const { body } = response;

            // check data
            expect(body.name).toBe(name);
            expect(body.actionId.text).toBe(reply);
            expect(body.messages.length).toBeGreaterThanOrEqual(1);
        })
    });
});
